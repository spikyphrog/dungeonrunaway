This is project is called DungeonRunaway,
developed for an individual project as part of my Game Design and Development BCs with Hons at the University of Bradford.

This game is inspired by the arcade game Pac-Man. 

Hristo Stoyanov 2021

Get in touch with me on Discord - Fblthp#9326

Link to the game can be found [here](https://spikyphrog.itch.io/dungeon-runaway)